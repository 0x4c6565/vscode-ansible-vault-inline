# Change Log

All notable changes to the `ansible-vault-inline` extension will be documented in this file.

## [Unreleased]

## [0.2.0] - 2019-10-22
### Changed
- Better documentation
- Displayed command name in contextual menu
### Fix
- Command show up in command palette

## [0.1.0] - 2019-10-21
### Added
- Initial release
